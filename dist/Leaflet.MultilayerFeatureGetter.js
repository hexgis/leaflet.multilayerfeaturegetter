/**
 * Created by Igor Castilheiro on 24/03/2016.
 */

L.MultilayerFeatureGetter = L.Class.extend({

    initialize: function (map) {
        // Triggered when the layer is added to a map.
        //   Register a click listener, then do all the upstream WMS things
        this._map = map;
        this.enable();
    },

    enable: function () {
        this._map.on('click', this.getFeatureInfo, this);
    },

    disable: function () {
        this._map.off('click', this.getFeatureInfo, this);
    },

    getFeatureInfo: function (evt) {
        // Make an AJAX request to the server and hope for the best
        this._hasPopup = false;
        this._popupContent = '';
        this._map.eachLayer(function (layer) {
            if (layer.hasOwnProperty("wmsParams")) {
                var url = this.getFeatureInfoUrl(evt.latlng, layer),
                    showResults = L.bind(this.showGetFeatureInfo, this);
                // showResults = L.bind(this.showGetFeatureInfo, layer.extras.title);
                fetch(url).
                    then(function (response) {
                        response.json().then(function(result){
                            if (layer.extras)
                                result.title = layer.extras.title;
                            showResults(null, evt.latlng, result);
                        })
                    }).catch(
                        function (error) {
                            showResults(error);
                        }
                    )
            }
        }, this
        );
    },

    getFeatureInfoUrl: function (latlng, layer) {
        // Construct a GetFeatureInfo request URL given a point
        var point = this._map.latLngToContainerPoint(latlng, this._map.getZoom()),
            size = this._map.getSize(),

            params = {
                request: 'GetFeatureInfo',
                service: 'WMS',
                srs: 'EPSG:4326',
                styles: layer.wmsParams.styles,
                transparent: layer.wmsParams.transparent,
                version: layer.wmsParams.version,
                format: layer.wmsParams.format,
                bbox: layer._map.getBounds().toBBoxString(),
                feature_count: 50,
                height: size.y,
                width: size.x,
                layers: layer.wmsParams.layers,
                query_layers: layer.wmsParams.layers,
                info_format: 'application/json'
            };

        params[params.version === '1.3.0' ? 'i' : 'x'] = point.x;
        params[params.version === '1.3.0' ? 'j' : 'y'] = point.y;

        return layer._url + L.Util.getParamString(params, layer._url, true);
    },

    showGetFeatureInfo: function (err, latlng, content) {
        if (err) {
            console.log(err);
            return;
        } // do nothing if there's an error
        var html = '';
        if (content.features.length > 0) {
            for(var feature in content.features) {
                if (content.title)
                    html += '<h4>' + content.title + '</h4>';

                html += '<table class="table">';

                for (var property in content.features[feature].properties) {
                    var propertyContent = content.features[feature].properties[property];
                    html += '<tr>';
                    html += '<td><b>' + property + '</b></td>';
                    html += '<td title="' + propertyContent + '">' + propertyContent + '</td>';
                    html += '</tr>';
                }
                
                html += '</table>';
                html += '<hr/>';
            }

            this._popupContent += html;

            if (!this._hasPopup) {
                L.popup({
                    maxWidth: 300,
                    maxHeight: 150,
                    closeButton: true,
                    closeOnClick: true
                })
                    .setLatLng(latlng)
                    .setContent(html)
                    .openOn(this._map);

                this._hasPopup = true;
            }
            else {
                this._map._popup.setContent(this._popupContent);
            }
        }

    }

});

L.multilayerFeatureGetter = function (map) {
    return new L.MultilayerFeatureGetter(map);
};
